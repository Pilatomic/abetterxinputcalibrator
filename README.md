# ABetterXinputCalibrator

A tool to calibrate touchscreens / graphic tablets with built-in display.

Meant to provide an alternative to xinput_calibrate, which does not work properly on multi-monitor setups.

Only works with Xinput based distros. ( Linux Mint 20 / Ubuntu 20 works ).

# Screenshot

![](doc/Screenshot.png)

# Compiling

    $ sudo apt-get install git build-essential qt5-default
    $ cd src
    $ qmake
    $ make

# Usage

1. Launch ABetterXinputCalibrator
2. Select the screen to display the calibration window
3. Select the input device to use
4. Click OK
5. Point the red cross. 
6. Calibration completes after each cross was pointed.
7. The selected devices are saved, and will be the default choices the next time the tool is started.

# How to find the correct input device

Listing available input devices : 

    $ xinput --list
	
Testing a device : 

	$ xinput --test DEVICE_ID
	
With DEVICE_ID the id of the device obtained with the previous command.

You found the correct device if lines are displayed when touching it. example :
 
    $ xinput --test 9
	motion a[0]=27216 a [1]=39220
	motion a[0]=27216 a [1]=39221
	motion a[0]=27216 a [1]=39222
	...

  
Warning : some device will not be listed before being touched a first time after being powered on.


