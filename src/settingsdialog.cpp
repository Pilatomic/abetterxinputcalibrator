#include "settingsdialog.h"
#include "ui_settingsdialog.h"
#include "inputdevice.h"
#include <QScreen>

SettingsDialog::SettingsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SettingsDialog)
{
    ui->setupUi(this);
    this->setModal(true);
    connect(this, &SettingsDialog::accepted, this, &SettingsDialog::onAccepted);
}

void SettingsDialog::setDisplayDeviceList(QStringList list, int preferredIndex)
{
    ui->displayBox->addItems(list);
    ui->displayBox->setCurrentIndex(preferredIndex);
}

void SettingsDialog::setInputDevicesList(QList<InputDevice> list, int preferredIndex)
{
    for(auto i = list.cbegin() ; i != list.cend() ; i++)
    {
        ui->inputBox->addItem(i->label, i->id);
    }
    if(preferredIndex >= 0)
    {
        ui->inputBox->setCurrentIndex(preferredIndex);
    }
}

SettingsDialog::~SettingsDialog()
{
    delete ui;
}

void SettingsDialog::onAccepted()
{
    emit settingsAccepted(ui->displayBox->currentText(), ui->inputBox->currentIndex());
}
