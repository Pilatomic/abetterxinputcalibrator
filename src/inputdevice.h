#ifndef INPUTDEVICE_H
#define INPUTDEVICE_H

#include <QString>

struct InputDevice
{
    QString id;
    QString label;
};

#endif // INPUTDEVICE_H
