#ifndef CALIBRATORWINDOW_H
#define CALIBRATORWINDOW_H

#include <QWidget>

#define POINT_COUNT     4       // Not more than 4 due to using 4x4matrix

class CalibratorWindow : public QWidget
{
    Q_OBJECT

public:
    CalibratorWindow(const QScreen* screen);
    void setCurrCrossPoint(int i);
    QPoint getCurrCrossPointGlobalCoordinates();

signals:
    void closed();
    void mousePressHappened(QPoint globalPos);

    // QWidget interface
protected:
    void closeEvent(QCloseEvent *event) override;
    void paintEvent(QPaintEvent *event) override;
    void mousePressEvent(QMouseEvent *event) override;

private:
    const QScreen* screen;
    QVector<QPoint> crossPoints;
    int currPointIdx = -1;

};
#endif // CALIBRATORWINDOW_H
