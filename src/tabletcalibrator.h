#ifndef TABLETCALIBRATOR_H
#define TABLETCALIBRATOR_H

#include "inputdevice.h"

#include <QObject>
#include <QMap>
#include <QVector>
#include <QProcess>

class CalibratorWindow;
class QScreen;
class SettingsDialog;
class QSettings;

class TabletCalibrator : public QObject
{
    Q_OBJECT
public:
    explicit TabletCalibrator(QObject *parent = nullptr);
    ~TabletCalibrator();

signals:

private:
    QMap<QString, CalibratorWindow*> calWindowsMap;
    SettingsDialog *settingDialog = nullptr;
    QProcess *xinputProcess = nullptr;
    CalibratorWindow* correctWindow = nullptr;
    int currCalibPoint = -1;
    QVector<QPair<QPoint, QPoint>> calibrationPoints;
    QSettings *savedSettings;
    QList<InputDevice> inputDevices;
    int selectedInputIndex = -1;


    void onAnyWindowClosed();
    void onXinputProcessFinished(int exitCode, QProcess::ExitStatus exitStatus);
    void onXinputProcessError(QProcess::ProcessError);
    void onSettingsAccepted(QString display, int inputIndex);
    void proceedToNextCalibPoint();
    void onTouchDetected(QPoint globalPos);
    int findSavedDisplayIndex(QStringList *list);
    int findSavedInputIndex(QList<InputDevice> *list);

    void applyXinputTfMatrixForInputDevice(QVector<float> values);
    QVector<float> computeNewTransformationMatrix();

    static QString trimLabelForSaveFile(QString s);
};

#endif // TABLETCALIBRATOR_H
