#include "tabletcalibrator.h"
#include "calibratorwindow.h"
#include "settingsdialog.h"
#include "inputdevice.h"

#include <QGuiApplication>
#include <QScreen>
#include <QProcess>
#include <QMessageBox>
#include <QDebug>
#include <QMatrix4x4>
#include <QVector4D>
#include <QSettings>
#include <QRegularExpression>

#define XINPUT_BINARY       "xinput"
#define XINPUT_LIST_ARGS    {"--list"}
#define XINPUT_TFMATRIX     "Coordinate Transformation Matrix"
#define XINPUT_SETPROP      "set-prop"

#define SAVEDKEY_DISPLAY    "display"
#define SAVEDKEY_INPUTNAME  "inputName"
#define SAVEDKEY_INPUTID    "inputIndex"

TabletCalibrator::TabletCalibrator(QObject *parent) : QObject(parent)
{
    const QScreen* primaryScreen = QGuiApplication::primaryScreen();
    QList<QScreen*> screens = QGuiApplication::screens();
    QStringList displayList;
    CalibratorWindow* windowOnPrimaryScreen = nullptr;

    foreach(const QScreen *screen, screens)
    {
        CalibratorWindow* calWindow = new CalibratorWindow(screen);
        displayList.append(screen->name());
        calWindowsMap.insert(screen->name(), calWindow);
        if(screen == primaryScreen) windowOnPrimaryScreen = calWindow;

        connect(calWindow, &CalibratorWindow::closed, this, &TabletCalibrator::onAnyWindowClosed);
        connect(calWindow, &CalibratorWindow::mousePressHappened, this, &TabletCalibrator::onTouchDetected);
    }

    savedSettings = new QSettings(QSettings::IniFormat, QSettings::UserScope, QString(), "ABetterXinputCalibrator", this);

    settingDialog = new SettingsDialog(windowOnPrimaryScreen);
    int savedDisplayIndex = findSavedDisplayIndex(&displayList);
    settingDialog->setDisplayDeviceList(displayList, savedDisplayIndex);
    connect(settingDialog, &SettingsDialog::rejected, this, &TabletCalibrator::onAnyWindowClosed);
    connect(settingDialog, &SettingsDialog::settingsAccepted, this, &TabletCalibrator::onSettingsAccepted);

    //Launch xinput list process, and display setting dialod when it completes
    inputDevices.clear();
    xinputProcess = new QProcess(this);
    connect(xinputProcess, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
            this, &TabletCalibrator::onXinputProcessFinished);
    connect(xinputProcess, &QProcess::errorOccurred, this, &TabletCalibrator::onXinputProcessError);
    xinputProcess->start(XINPUT_BINARY, QStringList(XINPUT_LIST_ARGS));

}

TabletCalibrator::~TabletCalibrator()
{
    qDeleteAll(calWindowsMap);
}

void TabletCalibrator::onAnyWindowClosed()
{
    exit(-1);
}

void TabletCalibrator::onXinputProcessFinished(int exitCode, QProcess::ExitStatus exitStatus)
{
    if(exitCode == 0 && exitStatus == QProcess::NormalExit)
    {
        QStringList inputDevicesStrings = QString(xinputProcess->readAllStandardOutput()).split("\n", QString::SkipEmptyParts);
        QString pattern("^(?<label>.*)\\tid=(?<id>\\d+)\\t.*$");
        QRegularExpression re(pattern);
        for(auto s = inputDevicesStrings.cbegin() ; s != inputDevicesStrings.cend() ; s++)
        {
            QString a = *s;
            QRegularExpressionMatch match = re.match(*s);
            if(match.hasMatch())
            {
                InputDevice id;
                id.label = match.captured("label");
                id.id = match.captured("id");
                inputDevices.append(id);
            }

        }
        int savedInputIndex = findSavedInputIndex(&inputDevices);
        settingDialog->setInputDevicesList(inputDevices, savedInputIndex);
    }
    settingDialog->show();
    xinputProcess->deleteLater();
}

//Needed to offer user a graphical way to quit app
void TabletCalibrator::onXinputProcessError(QProcess::ProcessError)
{
    settingDialog->show();
    xinputProcess->deleteLater();
}

void TabletCalibrator::onSettingsAccepted(QString display, int inputIndex)
{
    selectedInputIndex = inputIndex;
    correctWindow = calWindowsMap.value(display, nullptr);
    if(!correctWindow) exit(-1);

    // Save selected device
    InputDevice selectedDevice = inputDevices.at(selectedInputIndex);
    savedSettings->setValue(SAVEDKEY_DISPLAY, display);
    savedSettings->setValue(SAVEDKEY_INPUTID, selectedDevice.id);
    savedSettings->setValue(SAVEDKEY_INPUTNAME, trimLabelForSaveFile(selectedDevice.label));
    savedSettings->sync();

    //Restore identity transformation matrix for input device
    QVector<float> id({ 1.0, 0.0, 0.0,
                        0.0, 1.0, 0.0,
                        0.0, 0.0, 1.0 });
    applyXinputTfMatrixForInputDevice(id);

    proceedToNextCalibPoint();

}

void TabletCalibrator::proceedToNextCalibPoint()
{
    currCalibPoint++;
    correctWindow->setCurrCrossPoint(currCalibPoint);
}

void TabletCalibrator::onTouchDetected(QPoint globalPos)
{
    if(currCalibPoint < 0 || currCalibPoint >= POINT_COUNT) return;

    QPoint expectedPos = correctWindow->getCurrCrossPointGlobalCoordinates();
    calibrationPoints.append(QPair<QPoint, QPoint>(expectedPos, globalPos));

    if(currCalibPoint == (POINT_COUNT-1))
    {
        QVector<float> newTfMatrix = computeNewTransformationMatrix();
        applyXinputTfMatrixForInputDevice(newTfMatrix);
        exit(0);
    }
    else proceedToNextCalibPoint();
}

int TabletCalibrator::findSavedDisplayIndex(QStringList *list)
{
    QString s = savedSettings->value(SAVEDKEY_DISPLAY, QString()).toString();
    return s.isEmpty() ? -1 : list->indexOf(s);
}

int TabletCalibrator::findSavedInputIndex(QList<InputDevice> *list)
{
    QString savedLabel = savedSettings->value(SAVEDKEY_INPUTNAME, QString()).toString();
    QString savedId = savedSettings->value(SAVEDKEY_INPUTID, QString()).toString();
    if(savedLabel.isEmpty()) return -1;

    int matchIdx = -1;
    for(int i = 0 ; i < list->size() ; i++)
    {
        // Match on 1st label match or if label AND if matches
        QString trimmedLabel = trimLabelForSaveFile(list->at(i).label);
        if((trimmedLabel == savedLabel) && ( (matchIdx == -1) || (list->at(i).id == savedId)))
        {
            matchIdx = i;
        }
    }
    return matchIdx;
}

void TabletCalibrator::applyXinputTfMatrixForInputDevice(QVector<float> values)
{
    QStringList args;
    args.append(XINPUT_SETPROP);
    args.append(inputDevices.at(selectedInputIndex).id);
    args.append(XINPUT_TFMATRIX);
    for(auto it = values.constBegin() ; it != values.constEnd(); it++)
    {
        args.append(QString::number(*it));
    }
    QProcess::execute(XINPUT_BINARY, args);
}

QVector<float> TabletCalibrator::computeNewTransformationMatrix()
{
    QSize desktopSize = QGuiApplication::primaryScreen()->virtualSize();

    // FROM MAXIM AN 5296 : https://pdfserv.maximintegrated.com/en/an/AN5296.pdf
    QMatrix4x4 z;
    for(int i = 0 ; i < POINT_COUNT ; i++)
    {
        QPoint actual = calibrationPoints.at(i).second;
        z.setRow(i, QVector4D(actual.x(), actual.y(), 1.0, i == 3 ? 1.0 : 0.0));
    }

    QMatrix4x4 zt = z.transposed();
    QMatrix4x4 zwtf = (zt * z).inverted() * zt;

    QVector4D xd;
    for(int i = 0 ; i < POINT_COUNT ; i++)
    {
        QPoint expected = calibrationPoints.at(i).first;
        xd[i] = expected.x();
    }
    QVector4D xcoeffs = zwtf.map(xd) ;

    QVector4D yd;
    for(int i = 0 ; i < POINT_COUNT ; i++)
    {
        QPoint expected = calibrationPoints.at(i).first;
        yd[i] = expected.y();
    }
    QVector4D ycoeffs = zwtf.map(yd) ;


    QVector<float> vf({ xcoeffs.x(), xcoeffs.y(), xcoeffs.z() / desktopSize.width(),
                        ycoeffs.x(), ycoeffs.y(), ycoeffs.z() / desktopSize.height(),
                        0.0, 0.0, 1.0 });

    qDebug() << "New transformation matrix" << vf;

    return vf;
}

QString TabletCalibrator::trimLabelForSaveFile(QString s)
{
    return s.toLatin1().replace("?", "").trimmed();
}

