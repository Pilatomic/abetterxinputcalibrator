#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include "inputdevice.h"

#include <QDialog>

namespace Ui {
class SettingsDialog;
}

class SettingsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SettingsDialog(QWidget *parent = nullptr);
    void setDisplayDeviceList(QStringList list, int preferredIndex);
    void setInputDevicesList(QList<InputDevice> list, int preferredIndex);
    ~SettingsDialog();

signals:
    void settingsAccepted(QString display, int inputIndex);

private:
    Ui::SettingsDialog *ui;

    void onAccepted();
};

#endif // SETTINGSDIALOG_H
