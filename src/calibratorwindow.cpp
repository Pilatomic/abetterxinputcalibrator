#include "calibratorwindow.h"

#include <QScreen>
#include <QPainter>
#include <QMouseEvent>

#define POINTS_SCALE    10

CalibratorWindow::CalibratorWindow(const QScreen *screen)
{
    this->screen = screen;
    setWindowFlag(Qt::FramelessWindowHint, true);
    setGeometry(screen->geometry());
    showFullScreen();
}

void CalibratorWindow::setCurrCrossPoint(int i)
{
    currPointIdx = i;
    update();
}

QPoint CalibratorWindow::getCurrCrossPointGlobalCoordinates()
{
    return mapToGlobal(crossPoints.at(currPointIdx));
}


void CalibratorWindow::closeEvent(QCloseEvent *event)
{
    Q_UNUSED(event)
    emit closed();
}


void CalibratorWindow::paintEvent(QPaintEvent *)
{
    QPainter painter(this);

    QFont textFont;
    textFont.setPointSize(30);
    painter.setFont(textFont);
    painter.drawText(this->rect(), Qt::AlignCenter, screen->name());

    int x1 = width() / POINTS_SCALE;
    int x2 = width() * (POINTS_SCALE - 1) / POINTS_SCALE;
    int y1 = height() / POINTS_SCALE;
    int y2 = height() * (POINTS_SCALE - 1) / POINTS_SCALE;

    if(currPointIdx > -1)
    {
        crossPoints.clear();
        crossPoints.append(QPoint(x1, y1));
        crossPoints.append(QPoint(x2, y1));
        crossPoints.append(QPoint(x1, y2));
        crossPoints.append(QPoint(x2, y2));

        for(int i = 0 ; i < crossPoints.length() ; i++)
        {
            const int radius = 10;
            const QPoint p = crossPoints.at(i);
            painter.setPen(i == currPointIdx ? Qt::red : Qt::gray);
            painter.drawLine(p.x() - radius, p.y(), p.x() + radius, p.y());
            painter.drawLine(p.x(), p.y() - radius, p.x(), p.y() + radius);
        }
    }
}


void CalibratorWindow::mousePressEvent(QMouseEvent *event)
{
    if(event->buttons() == Qt::LeftButton)
    {
        emit mousePressHappened(event->globalPos());
    }
}
